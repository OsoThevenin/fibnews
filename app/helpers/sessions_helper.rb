module SessionsHelper
    def is_logged_in?
        !current_user.nil?
    end
end
