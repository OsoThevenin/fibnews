class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :link
  acts_as_votable
  belongs_to :commentable, polymorphic: true, optional:true
  has_many :comments, as: :commentable
end
