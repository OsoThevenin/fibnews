class Link < ApplicationRecord
    acts_as_votable
    belongs_to :user
    has_many :comments, as: :commentable
    validates :title, presence:true
    validate :text_or_url_but_not_both
    validates :url, uniqueness: true, 
        format: { with: /\A#{URI::regexp(['http', 'https'])}\z/, message: "Ha de ser una URL vàlida"},
        if: :url?

    def text_or_url_but_not_both
        if url.blank? and text.blank?
            errors[:base] << "Specify the Url or the Text, but not both"
        elsif url? and text?
            errors[:base] << "Specify only the Url or the Text, but not both"
        end
    end
end
