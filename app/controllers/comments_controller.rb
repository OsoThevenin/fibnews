class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  include SessionsHelper
  
  def new
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comment = Comment.new
  end

  def edit
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    if @comment.user_id != current_user.id
      redirect_to "/links/"
    end
  end

  def threads
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comments = Comment.all.where(user_id: current_user.id).order(created_at: :desc)
  end

  def voted_comments
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comments = Comment.all.select {|c| current_user.voted_for? c}
end

  def create
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @link = Link.find(params[:link_id])
    @comment = @link.comments.new(comment_params)
    @comment.user_id = current_user.id
    @comment.link_id = @link.id

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @link, notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def doReply
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comment = Comment.find(params[:comment_id])
    reply = @comment.comments.new(reply_params)
    reply.user_id = current_user.id
    reply.link_id = @comment.link_id

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment.link, notice: 'Reply was successfully created.' }
        format.json { render json: reply, status: :created, location: reply }
      else
        format.html { render action: "new" }
        format.json { render json: reply.errors, status: :unprocessable_entity }
      end
    end
  end

  def upvote
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comment = Comment.find(params[:id])
    @comment.upvote_by current_user
    redirect_back fallback_location: root_path
  end

  def downvote
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @comment = Comment.find(params[:id])
    @comment.unvote_by current_user
    redirect_back fallback_location: root_path
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment.link, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:link_id, :body, :user_id)
    end

    def reply_params
      params.require(:comment).permit(:id, :body, :user_id)
    end
end
