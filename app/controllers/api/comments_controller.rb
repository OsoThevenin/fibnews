module Api
    class Api::CommentsController < Api::ApplicationController
        #skip_before_action :verify_authenticity_token

        def show
    		id = params[:id].to_i
            @comment = Comment.find_by(id: id)
            if @comment == nil
                render json: {status: "Comment not found"}, status: 404
            else
                render json: {status: 'SUCCESS', message:'Loaded specific comment', data: @comment}, status: 200
            end
        end

        def threads
            #api_token = request.headers["Authorization"]
            #user = User.find_by(api_key: api_token)
            userid = 1
            @comments = Comment.all.where(user_id: userid).order(created_at: :desc)
            @size = @comments.size
			if @size == 0
				render json: {status: 'SUCCESS', message: 'There are no comments'}, status: 200
			else
				render json: {status: 'SUCCESS', message: 'Loaded all comments', data: @comments}, status: 200
			end
        end

        def voted_comments
            id = params[:id].to_i
            user = User.find_by(id: id)
            @comments = Comment.all.select {|c| current_user.voted_for? c}
            @size = @comments.size
			if @size == 0
				render json: {status: 'SUCCESS', message: 'There are no comments'}, status: 200
			else
				render json: {status: 'SUCCESS', message: 'Loaded all voted comments', data: @comments}, status: 200
			end
        end

        def new
            #api_token = request.headers["Authorization"]
            content = params[:content]
			link_id = params[:submissionid]
			parent_id = params[:parentid]
			if !Link.exists?(id: link_id)
				render json: {error: "Link doesn't exist"}, status: 404 and return
			elsif !parent_id.nil? && parent_id != ""
				@commentPare = Comment.find_by(id: parent_id)
				if @commentPare.nil?
					render json: {error: "Comment to reply doesn't exist"}, status: 404 and return
				end
                #user = User.find_by(api_key: api_token)
                userid = 1
                c = @commentPare.comments.new(body: content, user_id: userid, link_id: link_id, cached_votes_up: nil)
                unless c.save
					render json: {error: "Internal server error"}, status: 500 and return
				end
			elsif content.nil? || content == ""
				render json: {error: "Empty content"}, status: 404 and return
			else
				user = User.find_by(api_key: api_token)
				c = Comment.new(body: content, user_id: userid, link_id: link_id, cached_votes_up: nil, commentable_id: parent_id)
				unless c.save
					render json: {error: "Internal server error"}, status: 500 and return
				end
			end
			render json: {status: "SUCCESS", id: c.id}, status: 201
        end
        
        def delete
            #api_token = request.headers["Authorization"]
            id = params[:id].to_i
            @comment = Comment.find_by(id: id)
            if @comment == nil
            	render json: {error: "Resource doesn't exist"}, status: 404 and return
            else 
                #id2 = User.find_by(api_key: api_token).id
                userid = 1
                if @comment.user_id != userid
                    render json: {error: "User not authorized to delete"}, status: 401 and return
                else
                    @comment.destroy
                    render json: {status: 'SUCCESS', message:'Comment deleted'}, status: 200 and return
                end
		    end
        end
        
    end      
end        