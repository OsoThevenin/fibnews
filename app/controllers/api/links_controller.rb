require 'uri'

module Api
	class Api::LinksController < Api::ApplicationController
        #skip_before_action :verify_authenticity_token

        def show
    		id = params[:id].to_i
            @link = Link.find_by(id: id)
            if @link == nil
                render json: {status: "Link not found"}, status: 404
            else
                @comment = Comment.where(link_id: id)
                @size = @comment.size
                if @size == 0
                    render json: {status: 'SUCCESS', message:'Loaded specific link', data: @link, comments: 'there are no comments'}, status: 200
				else
                    render json: {status: 'SUCCESS', message:'Loaded specific link', data: @link, comments: @comment}, status: 200
                end
            end
        end
		
		#GET /submissions
        def index
			@link = Link.all
			@size = @link.size
			if @size == 0
				render json: {status: 'SUCCESS', message: 'There are no submissions'}, status: 200
			else
				render json: {status: 'SUCCESS', message: 'Loaded all submissions', data: @link}, status: 200
			end
        end
        
        def ask
            @link = Link.all.where(url: '')
            @size = @link.size
			if @size == 0
				render json: {status: 'SUCCESS', message: 'There are no submissions'}, status: 200
			else
				render json: {status: 'SUCCESS', message: 'Loaded all submissions', data: @link}, status: 200
			end
        end

        def new_index
            @link = Link.all.order("created_at DESC")
            @size = @link.size
			if @size == 0
				render json: {status: 'SUCCESS', message: 'There are no submissions'}, status: 200
			else
				render json: {status: 'SUCCESS', message: 'Loaded all submissions', data: @link}, status: 200
			end
        end

        def delete
            #api_token = request.headers["Authorization"]
			id = params[:id].to_i
            @link = Link.find_by(id: id)
            if @link == nil
            	render json: {error: "Resource doesn't exist"}, status: 404 and return
            else 
				#id2 = User.find_by(api_key: api_token).id
				id2= 1
                if @link.user_id != id2
                    render json: {error: "User not authorized to delete"}, status: 401 and return
                else
                    Comment.where(link_id: id).destroy_all
                    @link.destroy
                    render json: {status: 'SUCCESS', message:'Link deleted'}, status: 200 and return
                end
		    end
		end

        def submit
            #api_token = request.headers["Authorization"]
            title = params[:title].to_s
            content = params[:body].to_s
            type = params[:type].to_s

            if type.nil? || type == ""
				render json: {error: "No link type specified"}, status: 400 and return
			elsif type != 'Url' and type != 'Ask'
				render json: {error: "Invalid link type"}, status: 400 and return
			elsif title.nil? || title == ""
				render json: {error: "No title specified"}, status: 400 and return
			elsif content.nil? || content == ""
				render json: {error: "Empty body"}, status: 400 and return
			elsif type == 'Url' and not valid_url?(content)
				render json: {error: "Invalid link"}, status: 400 and return
			elsif type == 'Url' and Link.exists?(:url => content)
				linkid = Link.where(url: content)[0].id
				linklink = "#{request.protocol}#{request.host_with_port}" + "/links/#{linkid}"
				render json: {error: "Link already exists", id: linkid, link: linklink}, status: 303 and return
            end
            
			#userid = User.find_by(api_key: api_token).id
			userid = 1
            if type == 'Url'
                c = Link.new(title: title, user_id: userid, url: content, text: nil)
            else
                c = Link.new(title: title, user_id: userid, url: nil, text: content)
            end

			unless c.save
				render json: {error: "Internal server error"}, status: 500 and return
			end

			link = "#{request.protocol}#{request.host_with_port}" + "/links/#{c.id}"
			render json: {status: "SUCCESS", id: c.id, link: link}, status: 201
        end


        private
			def valid_url?(uri)
				uri = URI.parse(uri)
				uri.is_a?(URI::HTTP) && !uri.host.nil?
			rescue URI::InvalidURIError
				false
			end
    end
end