require 'json'

module Api
	class Api::UsersController < Api::ApplicationController
		before_action :set_user, only: [:show, :edit, :update, :destroy]
		#skip_before_action :verify_authenticity_token

		def profile
			id = params[:id].to_i
			@user = User.find_by(id: id).as_json
			if @user == nil
				render json: {status: "User not found"}, status: 404
			else
                #TODO eliminar atributs que no volem de user
                render json: {status: 'SUCCESS', message:'Loaded specific user', data: @user}, status: 200
			end
		end
		
		def update_user
			#api_token = request.headers["Authorization"]
			id = params[:id].to_i
			
			about_j = params.permit(:about)
			name_j = params.permit(:username)
			about = about_j['about']
			name = name_j['username']

			#user = User.find_by(api_key: api_token)
			userid = 1
			if userid != id
				render json: {error: "Can't modify other user's info"}, status: 406
			elsif !name.present?
				render json: {error: "The username can't be empty "}, status: 400
			else
			user.update(name: name)
			user.update(about: about)
			render json: {status: 'SUCCESS', message:'Updated specific user', data: user}, status: 200
			end
		end

		def vote_submission
			#api_token = request.headers["Authorization"]
			link_id = params[:id].to_i
			value = params[:value].to_i

			if  value == nil
				render json: {error: "Missing value"}, status: 400
			elsif value.to_i != -1 && value.to_i != 1
				render json: {error: "Invalid value"}, status: 400
			elsif link_id == nil
				render json: {error: "Missing submission id"}, status: 400
			else
				#user = User.find_by(api_key: api_token)
				userid=1
				@link = Link.find_by(id: link_id)
				if(@link == nil)
					render json: {success: "Submission not found"}, status: 404
				elsif @link.user_id == userid
					render json: {success: "Cannot vote owned submissions"}, status: 400
				else
					if value.to_i == 1
						@link.upvote_by user
						render json: {success: "Submission has been upvoted"}, status: 200
					else
						@link.unvote_by user
						render json: {success: "Submission has been unvoted"}, status: 200
					end
				end
			end
		end

		def vote_comment
			#api_token = request.headers["Authorization"]
			comment_id = params[:id].to_i
			value = params[:value].to_i

			if  value == nil
				render json: {error: "Missing value"}, status: 400
			elsif value.to_i != -1 && value.to_i != 1
				render json: {error: "Invalid value"}, status: 400
			elsif comment_id == nil
				render json: {error: "Missing comment id"}, status: 400
			else
				#user = User.find_by(api_key: api_token)
				userid = 1
				@comment = Comment.find_by(id: comment_id)
				if(@comment == nil)
					render json: {success: "Comment not found"}, status: 404
				elsif @comment.user_id == userid
					render json: {success: "Cannot vote owned comments"}, status: 400
				else
					if value.to_i == 1
						@comment.upvote_by user
						render json: {success: "Comment has been upvoted"}, status: 200
					else
						@comment.unvote_by user
						render json: {success: "Comment has been unvoted"}, status: 200
					end
				end
			end
		end

		private
			def user_params
				params.require(:user).permit(:username, :about)
			end

    end
end    