class LinksController < ApplicationController
  before_action :set_link, only: [:show, :edit, :update, :destroy]
  include SessionsHelper
  # GET /links
  # GET /links.json
  def index
    @links = Link.all.where.not(url: '').order(cached_votes_up: :desc)
  end

  # GET /links/1
  # GET /links/1.json
  def show
    @link = Link.find(params[:id])
  end

  # GET /links/new_index
  def new_index
    @links = Link.all.order("created_at DESC")
  end

  # GET /links/new
  def new
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @link = Link.new
  end

  # GET /links/ask
  def ask
    @links = Link.all.where(url: '')
  end
  
  def my_links
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @links = Link.all.where(user_id: current_user.id)
end

def voted_links
  unless is_logged_in?
    redirect_to "/auth/google_oauth2"
    return
  end
  @links = Link.all.select {|l| current_user.voted_for? l}
end

  # GET /links/1/edit
  def edit
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    if @link.user_id != current_user.id
      redirect_to "/links/"
    end
  end

  # POST /links
  # POST /links.json
  def create
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    url_newlink = link_params[:url]
    if url_newlink != '' && Link.exists?(url: url_newlink)
      redirect_to ("/links/"+(Link.find_by(url: url_newlink).id).to_s)
      return
    end
    @link = Link.new(link_params)
    @link.user_id = current_user.id

    respond_to do |format|
      if @link.save
        format.html { redirect_to "/links/new_index", notice: 'Link was successfully created.' }
        format.json { render :show, status: :created, location: @link }
      else
        format.html { render :new }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /links/1
  # PATCH/PUT /links/1.json
  def update
    respond_to do |format|
      if @link.update(link_params)
        format.html { redirect_to @link, notice: 'Link was successfully updated.' }
        format.json { render :show, status: :ok, location: @link }
      else
        format.html { render :edit }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /links/1
  # DELETE /links/1.json
  def destroy
    @link.destroy
    respond_to do |format|
      format.html { redirect_to links_url, notice: 'Link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @link = Link.find(params[:id])
    @link.upvote_by current_user
    redirect_back fallback_location: root_path
  end

  def downvote
    unless is_logged_in?
      redirect_to "/auth/google_oauth2"
      return
    end
    @link = Link.find(params[:id])
    @link.unvote_by current_user
    redirect_back fallback_location: root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def link_params
      params.require(:link).permit(:title, :url, :text)
    end
end
