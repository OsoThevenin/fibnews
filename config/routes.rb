Rails.application.routes.draw do
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  get 'links/new_index', to: 'links#new_index'
  get "links/ask" => "links#ask"
  get 'comments/threads' => 'comments#threads'
  get 'comments/voted_comments' => 'comments#voted_comments'
  get 'links/my_links' => 'links#my_links'
  get 'links/voted_links' => 'links#voted_links'

  resources :sessions, only: [:create, :destroy]

  resources :comments do
    member do
      put "like",    to: "comments#upvote"
      put "dislike", to: "comments#downvote"
      post "reply", to: "comments#doReply"
    end
    resources :comments
  end

  resources :links do
    member do
      put "like",    to: "links#upvote"
      put "dislike", to: "links#downvote"
    end
    resources :comments
  end
  resources :users
  root to: "links#index"

  namespace 'api' do

    #LINKS
    root "links#index"
    get "/submissions" => "links#index"
    get "/ask" => "links#ask"
    get "/newest" => "links#new_index"
    get "/submissions/:id" => "links#show"
    post "/submissions" => "links#submit"
    delete "/submissions/:id" => "links#delete"

    #COMMENTS
    get "/threads" => "comments#threads"
    delete "/comments/:id" => "comments#delete"
    get "/comments/:id" => "comments#show"
    post "/comments" => "comments#new"

    # USERS
    get "/users/:id" => "users#profile"
    put "/users/:id" => "users#update_user"
    post "/vote_submission" => "users#vote_submission"
    post "/vote_comment" => "users#vote_comment"

  end

end
