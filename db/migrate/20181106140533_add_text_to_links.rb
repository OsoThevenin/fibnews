class AddTextToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :text, :string
  end
end
