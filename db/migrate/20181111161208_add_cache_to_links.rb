class AddCacheToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :cached_votes_up, :integer
  end
end
