class AddCacheToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :cached_votes_up, :integer
  end
end
